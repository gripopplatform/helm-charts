
# GENERATE AN UPDATED INDEX.YAML
Use Helm to generate an updated index.yaml file by passing in the directory path and the url of the remote repository to the helm repo index command like this:

helm repo index gripop/ --url https://bitbucket.org/gripopplatform/helm-charts.git

# Create a new version of the helm chart:
```
$ ~/projects/schulinck/helm-charts/gripop/charts (master) $ helm package ../
Successfully packaged chart and saved it to: ~/projects/schulinck/helm-charts/gripop/charts/gripop-0.0.1.tgz
```
Now commit and push the lot.


# Installing kubectl & helm
todo

# Add bash-autocompletion for kubectl and helm 
```
source <(kubectl completion bash) # setup autocomplete in bash into the current shell, bash-completion package should be installed first.
echo "source <(kubectl completion bash)" >> ~/.bashrc # add autocomplete permanently to your bash shell.
source <(helm completion bash)  
echo "source <(helm completion bash)" >> ~/.bashrc  
```

# Running make
Set the branch you're building and run make like so:
`branch=master make install` or `make install branch=master`
