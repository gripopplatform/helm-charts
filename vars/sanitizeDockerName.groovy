#!/usr/bin/env groovy

def call(String dockerBranchName) {
	def sanitized = dockerBranchName.toLowerCase().replaceAll("[/]|%2f","-")

	if(sanitized.size() > 42) {
		// Trim branch name and append digest to try and avoid naming collision
		def sha1 = sanitized.digest('SHA-1').substring(0,6)
		sanitized = sanitized.replaceAll("feature|hotfix", { x -> x[0] }).substring(0,35).concat('-').concat(sha1)
	}

	print(sanitized)

	return sanitized
}
